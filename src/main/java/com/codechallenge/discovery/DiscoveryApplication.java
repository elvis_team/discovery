package com.codechallenge.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * This class defines our microservices naming server
 * This service must be started first to coordinate requests between services.
 * It allows for easy load balancing implementation.
 *
 * @author  Elvis
 * @version 1.0, 10/05/18
 */
@SpringBootApplication
@EnableEurekaServer
public class DiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscoveryApplication.class, args);
	}
}
